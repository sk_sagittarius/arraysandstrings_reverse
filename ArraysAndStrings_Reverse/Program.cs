﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysAndStrings_Reverse
{
    class Program
    {
        static void Main(string[] args)
        {
            string numbers = Console.ReadLine();
            char[] numbers2 = numbers.ToCharArray();
            Array.Reverse(numbers2);
            Console.WriteLine(numbers2);
            Console.ReadLine();
        }
    }
}
